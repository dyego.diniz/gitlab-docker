# Project name: gitlab-docker

# objectives
This project shows how to create a pipeline in environments develop, staging and production. I will explain how to deploy in docker and we will use terraform to deploy infrastructure.

# necessary projects dependencies
- 

# requisites
- 

# to do
- make the environments with terraform on EC2
- build pipelines

# doing

# done
- 

# improvements
- 

# references
- 

# commands
- 
