resource "aws_vpc" "Main" {            
  cidr_block       = var.main_vpc_cidr 
  instance_tenancy = "default"
  tags = {
    "Name" = "vpc-tf-testes"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_subnet" "publicsubnets" { 
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.public_subnets 
  tags = {
    "Name" = "sub-tf-rede-pub-dev"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_internet_gateway" "IGW" { 
  vpc_id = aws_vpc.Main.id              
  tags = {
    "Name" = "igw-tf-rede-pub-dev"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_route_table" "PublicRT" { 
  vpc_id = aws_vpc.Main.id
  route {
    cidr_block = "0.0.0.0/0" 
    gateway_id = aws_internet_gateway.IGW.id
  }
  tags = {
    "Name" = "rt-tf-rede-pub-dev"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_route_table_association" "PublicRTassociation" {
  subnet_id      = aws_subnet.publicsubnets.id
  route_table_id = aws_route_table.PublicRT.id
}

resource "aws_eip" "nateIP" {
  vpc = true
}

###
# Public Security Group
##
 
resource "aws_security_group" "ec2-sg" {
  name = "tf-teste-ec2-sg"
  description = "Public internet access"
  vpc_id = aws_vpc.Main.id
 
  tags = {
    Name        = "tf-teste-ec2-sg"
    Role        = "public"
    Project     = "dev"
    Environment = "dev"
    "deploy" = "terraformdeploy"
  }
}
 
resource "aws_security_group_rule" "public_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "saida default"
}

resource "aws_security_group_rule" "public_in_interno" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["10.22.0.0/16"]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "acesso total da rede dentro da mesma vpc"
}

# resource "aws_security_group_rule" "public_in_ssh" {
#   type              = "ingress"
#   from_port         = 22
#   to_port           = 22
#   protocol          = "tcp"
#   cidr_blocks       = ["201.7.35.76/32"]
#   security_group_id = aws_security_group.ec2-sg.id
#   description       = "acesso ssh dyego"
# }

resource "aws_security_group_rule" "public_in_all" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = [var.public_ip]
  security_group_id = aws_security_group.ec2-sg.id
  description       = "acesso total dyego"
}

###
# Public Security Group
##