#! /bin/bash

echo '--------------------------------------------------------------atualizando so'
apt-get update

echo '--------------------------------------------------------------retirando o swapp para não dar erro no cluster com kubeadm'
swapoff -a && sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

echo '--------------------------------------------------------------instalando o docker segundo doc oficial https://docs.docker.com/engine/install/ubuntu/'
apt-get install ca-certificates curl gnupg lsb-release -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu   focal stable' | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io -y

echo '--------------------------------------------------------------instalando o docker compose seguindo doc oficial https://docs.docker.com/compose/install/'
curl -L 'https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64' -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# echo '--------------------------------------------------------------instalando o kubectl'
# apt-get install -y apt-transport-https ca-certificates curl
# curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
# echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
# apt-get update
# apt-get install -y kubectl
# mkdir /root/.kube
# touch /root/.kube/config

# echo '--------------------------------------------------------------instalando awscli'
# curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
# unzip awscliv2.zip
# sudo ./aws/install

echo '--------------------------------------------------------------instalando gitlab runner (seguindo https://www.fosstechnix.com/how-to-install-gitlab-runner-on-ubuntu/)'
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
apt-get install gitlab-runner
gitlab-runner -version
gitlab-runner status
echo 'gitlab-runner ALL=(ALL:ALL) ALL' >> /etc/sudoers
echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

echo '--------------------------------------------------------------permissões para comando docker e docker-compose'
usermod -aG docker ubuntu
usermod -aG docker vagrant
usermod -aG docker gitlab-runner

# echo '--------------------------------------------------------------install rancher'
# docker run -d --restart=unless-stopped \
#   -p 80:80 -p 443:443 \
#   -v /tmp/certificado/dyego-cloud-fullchain.pem:/etc/rancher/ssl/cert.pem \
#   -v /tmp/certificado/dyego-cloud-privkey.pem:/etc/rancher/ssl/key.pem \
#   --privileged \
#   rancher/rancher:v2.6.5 \
#   --no-cacerts