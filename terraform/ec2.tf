resource "aws_key_pair" "chave-tf-1" {
  key_name   = "chave-tf-1"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDO+kBKNX1XRahpE+Oa0cydV8fnb72fXLGt4h3+aMoX7Mm/7sfs650zI18H4GI5RtN8ZL45YwZIsl6NzKtkrxeOx1f40WncRihqIuBVdqAfruPnPeHyF1mVQSbc3Vjqi8vG8XjIUfIvmWFBzuQNaCd/x1EKT/lncxb555jOo7uFDr5kYWnQ78hWExsDyW/Ngcfxwy+IC/kWWRh65Xbf/2mIZ+94MIeYYC4L5Ux44fJy3uk54BUIIRSSpmDyKH/IAasyX2U3nBviI3/A5VI96nTfVnf5Es4vTjqGE98cpYyHF0G3x/ZCKZ05oeim+QjuEM4X/c39ZdKRIim95XB6YW8J ubuntu"
}

resource "aws_instance" "vm1-dev" {
  ami                         = var.instance_ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.chave-tf-1.key_name
  subnet_id                   = aws_subnet.publicsubnets.id
  associate_public_ip_address = true
  private_ip                  = "10.22.1.201"
  vpc_security_group_ids      = [aws_security_group.ec2-sg.id]

  user_data = file("script-inicial.sh")
  tags = {
    "Name"   = "vm1-dev"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_instance" "vm2-hml" {
  ami                         = var.instance_ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.chave-tf-1.key_name
  subnet_id                   = aws_subnet.publicsubnets.id
  associate_public_ip_address = true
  private_ip                  = "10.22.1.202"
  vpc_security_group_ids      = [aws_security_group.ec2-sg.id]

  user_data = file("script-inicial.sh")
  tags = {
    "Name"   = "vm2-hml"
    "deploy" = "terraformdeploy"
  }
}

resource "aws_instance" "vm3-prd" {
  ami                         = var.instance_ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.chave-tf-1.key_name
  subnet_id                   = aws_subnet.publicsubnets.id
  associate_public_ip_address = true
  private_ip                  = "10.22.1.203"
  vpc_security_group_ids      = [aws_security_group.ec2-sg.id]

  user_data = file("script-inicial.sh")
  tags = {
    "Name"   = "vm3-prd"
    "deploy" = "terraformdeploy"
  }
}